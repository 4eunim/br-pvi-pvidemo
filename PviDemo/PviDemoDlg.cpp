
//////////////////////////////////////////////////////////////////////////////
// PviDemoDlg.cpp: PVI sample dialog class for MFC applications
//////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PviDemo.h"
#include "PviConnectDlg.h"
#include "PviObjectsDlg.h"
#include "PviDemoDlg.h"

#include "PviCom.h"				// PVI Include File

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//////////////////////////////////////////////////////////////////////////////
// CPviDemoDlg dialog

CPviDemoDlg::CPviDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPviDemoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPviDemoDlg)
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_PviConnection = CONN_STATE_UNDEFINED;
	m_CpuConnection = CONN_STATE_UNDEFINED;

	// initialize pvi object controlling data:
	m_PviLineObj.pNameID = _T("line");
	m_PviLineObj.EditID = 0;
	m_PviLineObj.LinkID = NULL;
	m_PviDeviceObj.pNameID = _T("device");
	m_PviDeviceObj.EditID = 0;
	m_PviDeviceObj.LinkID = NULL;
	m_PviStationObj.pNameID = _T("station");
	m_PviStationObj.EditID = 0;
	m_PviStationObj.LinkID = NULL;
	m_PviCpuObj.pNameID = _T("cpu");
	m_PviCpuObj.EditID = 0;
	m_PviCpuObj.LinkID = NULL;
	m_PviTaskObj.pNameID = _T("task");
	m_PviTaskObj.EditID = 0;
	m_PviTaskObj.LinkID = NULL;
	m_PviPvarObj1.pNameID = _T("variable 1");
	m_PviPvarObj1.EditID = IDC_PVAR1;
	m_PviPvarObj1.LinkID = NULL;
	m_PviPvarObj2.pNameID = _T("variable 2");
	m_PviPvarObj2.EditID = IDC_PVAR2;
	m_PviPvarObj2.LinkID = NULL;
	m_PviPvarObj3.pNameID = _T("variable 3");
	m_PviPvarObj3.EditID = IDC_PVAR3;
	m_PviPvarObj3.LinkID = NULL;
	m_PviPvarObj4.pNameID = _T("variable 4");
	m_PviPvarObj4.EditID = IDC_PVAR4;
	m_PviPvarObj4.LinkID = NULL;
}

void CPviDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPviDemoDlg)
	//}}AFX_DATA_MAP
}

//////////////////////////////////////////////////////////////////////////////
// CPviDemoDlg pvi demo functions:

void CPviDemoDlg::DisplayDemoMessage (LPCTSTR pText, ...)
{
	va_list	argList;
	TCHAR	TextBf[1024];

	va_start (argList, pText);
	_vstprintf (TextBf, pText, argList);
	va_end (argList);

	CWnd::MessageBox (TextBf);
}

void CPviDemoDlg::DisplayConnectionState ()
{
	CString	PviStateText;
	CString	CpuStateText;

	switch (m_PviConnection)
	{
		case CONN_STATE_DISCONNECTED:
			PviStateText.Format (_T("PVI disconnected (%d)"), m_PviErrCode);
			break;

		case CONN_STATE_CONNECTED:
			PviStateText = _T("PVI connected");
			break;

		default:
			PviStateText = _T("");
	}
	
	switch (m_CpuConnection)
	{
		case CONN_STATE_DISCONNECTED:
			CpuStateText.Format (_T("CPU disconnected (%d)"), m_CpuErrCode);
			break;

		case CONN_STATE_CONNECTED:
			CpuStateText = _T("CPU connected");
			break;

		default:
			CpuStateText = _T("");
	}

	if ((! PviStateText.IsEmpty ()) && (! CpuStateText.IsEmpty ()))
		PviStateText += _T(" / ");
	PviStateText += CpuStateText;

	SetDlgItemText (IDC_STATIC_COMM, PviStateText);
}

void CPviDemoDlg::DisableDemoObjectControls ()
{
	CEdit*	pEditCtrl;

	pEditCtrl = (CEdit*) GetDlgItem (IDC_PVAR1);
	pEditCtrl->EnableWindow (FALSE);
	pEditCtrl->SetReadOnly (TRUE);
	pEditCtrl->SetWindowText (_T(""));

	pEditCtrl = (CEdit*) GetDlgItem (IDC_PVAR2);
	pEditCtrl->EnableWindow (FALSE);
	pEditCtrl->SetReadOnly (TRUE);
	pEditCtrl->SetWindowText (_T(""));

	pEditCtrl = (CEdit*) GetDlgItem (IDC_PVAR3);
	pEditCtrl->EnableWindow (FALSE);
	pEditCtrl->SetReadOnly (TRUE);
	pEditCtrl->SetWindowText (_T(""));

	pEditCtrl = (CEdit*) GetDlgItem (IDC_PVAR4);
	pEditCtrl->EnableWindow (FALSE);
	pEditCtrl->SetReadOnly (TRUE);
	pEditCtrl->SetWindowText (_T(""));
}

void CPviDemoDlg::CreateDemoObjects (CPviObjectsDlg&	Objects)
{
	//////////////////////////////////////////////////////////////////////////
	// Create all pvi process objects (temporary) and pvi link objects 
	// for this demo dialog window.
	// Object Hierarchy:
	//   line -> device -> station -> cpu -> task -> variables (1 - 4)
	// If no pvi name was specified in the objects dialog by the user no
	// pvi object will be created. The pvi object handle indicates if the
	// process object exists (!= NULL) or not (== NULL).

	CExString	PObjPvName;
	CExString	PObjName;
	CExString	PObjDesc;
	CExString	LinkDesc;
	LPARAM		lParam = 0;
	INT			ErrCode = 0;

	// Create all pvi objects with pathname mode:
	PObjName.Format (_T("%c%s"), _T(PVICHR_USEPATH), _T(PVIBASE_POBJNAME));

	// Prepare link descriptor for all variable objects:
#ifdef UNICODE
	LinkDesc.Format (_T("LT=prc VT=wstring VL=%u EV="), PVI_STRDATA_MAXLEN * 2);
#else
	LinkDesc.Format (_T("LT=prc VT=string VL=%u EV="), PVI_STRDATA_MAXLEN);
#endif

	//////////////////////////////////////////////////////////////////////////
	// pvi line object:
	if ((! Objects.m_NameLine.IsEmpty ()) && (ErrCode == 0))
	{
		PObjName += _T(PVICHR_PATH);
		PObjName += Objects.m_NameLine;
		PObjDesc.Format (_T("CD=\"%s\""), Objects.m_ConnectLine);
		lParam = (LPARAM) &m_PviLineObj;

		// create a temporary line object and a link object:
		ErrCode = PviCreateRequest (PObjName, POBJ_LINE, PObjDesc,
									PVI_HMSG_NIL, 0, 0, "EV=",
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi device object:
	if ((! Objects.m_NameDevice.IsEmpty ()) && (ErrCode == 0))
	{
		PObjName += _T(PVICHR_PATH);
		PObjName += Objects.m_NameDevice;
		PObjDesc.Format (_T("CD=\"%s\""), Objects.m_ConnectDevice);
		lParam = (LPARAM) &m_PviDeviceObj;

		// create a temporary device object and a link object:
		ErrCode = PviCreateRequest (PObjName, POBJ_DEVICE, PObjDesc,
									PVI_HMSG_NIL, 0, 0, "EV=",
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi station object:
	if ((! Objects.m_NameStation.IsEmpty ()) && (ErrCode == 0))
	{
		PObjName += _T(PVICHR_PATH);
		PObjName += Objects.m_NameStation;
		PObjDesc.Format (_T("CD=\"%s\""), Objects.m_ConnectStation);
		lParam = (LPARAM) &m_PviStationObj;

		// create a temporary station object and a link object:
		ErrCode = PviCreateRequest (PObjName, POBJ_STATION, PObjDesc,
									PVI_HMSG_NIL, 0, 0, "EV=",
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi cpu object:
	if ((! Objects.m_NameCpu.IsEmpty ()) && (ErrCode == 0))
	{
		PObjName += _T(PVICHR_PATH);
		PObjName += Objects.m_NameCpu;
		PObjDesc.Format (_T("CD=\"%s\""), Objects.m_ConnectCpu);
		lParam = (LPARAM) &m_PviCpuObj;

		// create a temporary cpu object and a link object:
		ErrCode = PviCreateRequest (PObjName, POBJ_CPU, PObjDesc,
									CWnd::m_hWnd, WM_PVI_CPU_EVENT, lParam, "EV=e",
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi task object:
	if ((! Objects.m_NameTask.IsEmpty ()) && (ErrCode == 0))
	{
		PObjName += _T(PVICHR_PATH);
		PObjName += Objects.m_NameTask;
		PObjDesc.Format (_T("CD=\"%s\""), Objects.m_ConnectTask);
		lParam = (LPARAM) &m_PviTaskObj;

		// create a temporary task object and a link object:
		ErrCode = PviCreateRequest (PObjName, POBJ_TASK, PObjDesc,
									PVI_HMSG_NIL, 0, 0, "EV=",
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi variable object 1:
	if ((! Objects.m_NamePv1.IsEmpty ()) && (ErrCode == 0))
	{
		PObjPvName = PObjName;
		PObjPvName += _T(PVICHR_PATH);
		PObjPvName += Objects.m_NamePv1;
		PObjDesc.Format (_T("CD=\"%s\" %s"), Objects.m_ConnectPv1, Objects.m_ParamPv1);
		lParam = (LPARAM) &m_PviPvarObj1;

		// create a temporary variable object and a link object:
		ErrCode = PviCreateRequest (PObjPvName, POBJ_PVAR, PObjDesc,
									CWnd::m_hWnd, WM_PVI_PVAR_EVENT, lParam, LinkDesc,
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi variable object 2:
	if ((! Objects.m_NamePv2.IsEmpty ()) && (ErrCode == 0))
	{
		PObjPvName = PObjName;
		PObjPvName += _T(PVICHR_PATH);
		PObjPvName += Objects.m_NamePv2;
		PObjDesc.Format (_T("CD=\"%s\" %s"), Objects.m_ConnectPv2, Objects.m_ParamPv2);
		lParam = (LPARAM) &m_PviPvarObj2;

		// create a temporary variable object and a link object:
		ErrCode = PviCreateRequest (PObjPvName, POBJ_PVAR, PObjDesc,
									CWnd::m_hWnd, WM_PVI_PVAR_EVENT, lParam, LinkDesc,
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi variable object 3:
	if ((! Objects.m_NamePv3.IsEmpty ()) && (ErrCode == 0))
	{
		PObjPvName = PObjName;
		PObjPvName += _T(PVICHR_PATH);
		PObjPvName += Objects.m_NamePv3;
		PObjDesc.Format (_T("CD=\"%s\" %s"), Objects.m_ConnectPv3, Objects.m_ParamPv3);
		lParam = (LPARAM) &m_PviPvarObj3;

		// create a temporary variable object and a link object:
		ErrCode = PviCreateRequest (PObjPvName, POBJ_PVAR, PObjDesc,
									CWnd::m_hWnd, WM_PVI_PVAR_EVENT, lParam, LinkDesc,
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	//////////////////////////////////////////////////////////////////////////
	// pvi variable object 4:
	if ((! Objects.m_NamePv4.IsEmpty ()) && (ErrCode == 0))
	{
		PObjPvName = PObjName;
		PObjPvName += _T(PVICHR_PATH);
		PObjPvName += Objects.m_NamePv4;
		PObjDesc.Format (_T("CD=\"%s\" %s"), Objects.m_ConnectPv4, Objects.m_ParamPv4);
		lParam = (LPARAM) &m_PviPvarObj4;

		// create a temporary variable object and a link object:
		ErrCode = PviCreateRequest (PObjPvName, POBJ_PVAR, PObjDesc,
									CWnd::m_hWnd, WM_PVI_PVAR_EVENT, lParam, LinkDesc,
									CWnd::m_hWnd, WM_PVI_CREATE_RESP, lParam);
	}

	if (ErrCode != 0)
		DisplayDemoMessage (_T("Fatal error on create request (%d)"), ErrCode);
}

void CPviDemoDlg::FreeDemoObjects ()
{
	//////////////////////////////////////////////////////////////////////////
	// Free all existing pvi link objects and the corresponding 
	// temporary pvi process objects for the demo dialog window.

	PviUnlinkAll (PVI_HMSG_NIL);
	PviUnlinkAll (CWnd::m_hWnd);

	// set all pvi handles to undefined state:
	m_PviLineObj.LinkID = NULL;
	m_PviDeviceObj.LinkID = NULL;
	m_PviStationObj.LinkID = NULL;
	m_PviCpuObj.LinkID = NULL;
	m_PviTaskObj.LinkID = NULL;
	m_PviPvarObj1.LinkID = NULL;
	m_PviPvarObj2.LinkID = NULL;
	m_PviPvarObj3.LinkID = NULL;
	m_PviPvarObj4.LinkID = NULL;
}

void CPviDemoDlg::CreateConnection (CPviConnectDlg&	Connect)
{
	//////////////////////////////////////////////////////////////////////////
	// Create the connection to pvi:

	INT			ErrCode;
	CExString	InitParam;

	// build the initialize parameter:
	if (Connect.m_CommRemote)
		// remote communication:
		InitParam.Format (_T("%s=%s %s=%u %s=%u"),
			_T(KWINIT_IPADDR), Connect.m_IpAddr,
			_T(KWINIT_IPPORT), Connect.m_Port,
			_T(KWINIT_COM_TIMEOUT), Connect.m_CommTimeout);
	else
		// local communication:
		InitParam.Format (_T("%s=%u"),
			_T(KWINIT_COM_TIMEOUT), Connect.m_CommTimeout);

	// initialize the pvi library instance and
	// establish the communication to pvi manager:
	ErrCode = PviInitialize (0, 0, InitParam, NULL);
	if (ErrCode != 0)
	{
		DisplayDemoMessage (_T("Can't initialize PVI (%d)"), ErrCode);
		return;
	}

	// set global pvi events to watch the connection to pvi:
	PviSetGlobEventMsg (POBJ_EVENT_PVI_CONNECT, CWnd::m_hWnd, WM_PVI_GLOBAL_EVENT, 0);
	PviSetGlobEventMsg (POBJ_EVENT_PVI_DISCONN, CWnd::m_hWnd, WM_PVI_GLOBAL_EVENT, 0);
	PviSetGlobEventMsg (POBJ_EVENT_PVI_ARRANGE, CWnd::m_hWnd, WM_PVI_GLOBAL_EVENT, 0);
}

void CPviDemoDlg::FreeConnection ()
{
	//////////////////////////////////////////////////////////////////////////
	// Frees the pvi connection.

	// free all created pvi objects:
	FreeDemoObjects ();

	// deinitialize pvi library instance:
	PviDeinitialize ();
}


BEGIN_MESSAGE_MAP(CPviDemoDlg, CDialog)
	//{{AFX_MSG_MAP(CPviDemoDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CONNECT, OnConnect)
	ON_BN_CLICKED(IDC_OBJECTS, OnObjects)
	ON_EN_SETFOCUS(IDC_PVAR1, OnSetfocusPvar1)
	ON_EN_SETFOCUS(IDC_PVAR2, OnSetfocusPvar2)
	ON_EN_SETFOCUS(IDC_PVAR3, OnSetfocusPvar3)
	ON_EN_SETFOCUS(IDC_PVAR4, OnSetfocusPvar4)
	ON_EN_KILLFOCUS(IDC_PVAR1, OnKillfocusPvar1)
	ON_EN_KILLFOCUS(IDC_PVAR2, OnKillfocusPvar2)
	ON_EN_KILLFOCUS(IDC_PVAR3, OnKillfocusPvar3)
	ON_EN_KILLFOCUS(IDC_PVAR4, OnKillfocusPvar4)
	ON_BN_CLICKED(IDC_ACTIVE, OnActive)
	ON_BN_CLICKED(IDC_IDLE, OnIdle)
	ON_BN_CLICKED(IDC_WRITE, OnWrite)
	//}}AFX_MSG_MAP

	// Messages for pvi:
	ON_MESSAGE (WM_PVI_GLOBAL_EVENT, OnPviGlobalEvent)
	ON_MESSAGE (WM_PVI_CPU_EVENT, OnPviCpuEvent)
	ON_MESSAGE (WM_PVI_PVAR_EVENT, OnPviPvarEvent)
	ON_MESSAGE (WM_PVI_CREATE_RESP, OnPviCreateResp)
	ON_MESSAGE (WM_PVI_WRITE_RESP, OnPviWriteResp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPviDemoDlg message handlers

BOOL CPviDemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon (m_hIcon, TRUE);				// set big icon
	SetIcon (m_hIcon, FALSE);				// set small icon

	m_bEventActive = TRUE;
	m_pActiveButton = (CButton*) GetDlgItem (IDC_ACTIVE);
	m_pIdleButton = (CButton*) GetDlgItem (IDC_IDLE);
	m_pActiveButton->EnableWindow (m_bEventActive == FALSE);
	m_pIdleButton->EnableWindow (m_bEventActive != FALSE);

	m_pWriteButton = (CButton*) GetDlgItem (IDC_WRITE);
	m_pWriteButton->EnableWindow (FALSE);
	m_pFocusCtrl = NULL;

	// disable object controls in dialog:
	DisableDemoObjectControls ();

	//////////////////////////////////////////////////////////////////////////
	// establish a connection to pvi:

	CPviConnectDlg ConnectDlg;

	// load the pvi connection parameter from the INI file:
	ConnectDlg.LoadParam();

	// create a new connection to pvi:
	CreateConnection (ConnectDlg);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPviDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CPviDemoDlg::OnPaint() 
{
#ifdef UNDER_CE
	// variante for windows CE
	CDialog::OnPaint();
#else
	// variante for windows
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
#endif
}

HCURSOR CPviDemoDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPviDemoDlg::OnDestroy() 
{
	CDialog::OnDestroy();

	//////////////////////////////////////////////////////////////////////////
	// Deinitialize pvi:

	// Deinitialize pvi library instance:
	PviDeinitialize ();
}

void CPviDemoDlg::OnConnect() 
{
	//////////////////////////////////////////////////////////////////////////
	// The "Connect" button was pressed in objects dialog:

	CPviConnectDlg ConnectDlg;

	// disable write button:
	m_pWriteButton->EnableWindow (FALSE);
	m_pFocusCtrl = NULL;

	// load the pvi connection parameter from the INI file:
	ConnectDlg.LoadParam();

	if (ConnectDlg.DoModal() == IDOK)
	{
		///////////////////////////////////////////////////
		// The OK button was pressed in connection dialog:

		// save the pvi connection parameter:
		ConnectDlg.SaveParam();

		// disable object controls in dialog:
		DisableDemoObjectControls ();

		// free all created pvi objects (if necessary):
		FreeDemoObjects ();

		// reset the display for communication state in demo dialog:
		SetDlgItemText (IDC_STATIC_COMM, _T(""));

		// free the connection to pvi:
		FreeConnection ();

		// display undefined pvi and cpu connection:
		m_PviConnection = CONN_STATE_UNDEFINED;
		m_CpuConnection = CONN_STATE_UNDEFINED;
		DisplayConnectionState ();

		// create a new connection to pvi:
		CreateConnection (ConnectDlg);
	}	
}

void CPviDemoDlg::OnObjects() 
{
	//////////////////////////////////////////////////////////////////////////
	// The "Objects" button was pressed in objects dialog:

	CPviObjectsDlg ObjectsDlg;

	// disable write button:
	m_pWriteButton->EnableWindow (FALSE);
	m_pFocusCtrl = NULL;

	// load the pvi object parameter from the INI file:
	ObjectsDlg.LoadParam();

	if (ObjectsDlg.DoModal() == IDOK)
	{
		////////////////////////////////////////////////////////
		// The OK button was pressed in objects dialog:

		// save the pvi object parameter:
		ObjectsDlg.SaveParam();

		// disable object controls in dialog:
		DisableDemoObjectControls ();

		// free all created pvi objects:
		FreeDemoObjects ();

		// display undefined cpu connection:
		m_CpuConnection = CONN_STATE_UNDEFINED;
		DisplayConnectionState ();

		if (m_PviConnection == CONN_STATE_CONNECTED)
			// create new pvi objects:
			CreateDemoObjects (ObjectsDlg);
	}
}

void CPviDemoDlg::OnSetfocusPvar1() 
{
	// adjust idle state for variable 1:
	PviWriteRequest (m_PviPvarObj1.LinkID, POBJ_ACC_EVMASK, NULL, 0, NULL, 0, 0);

	// enable write button:
	m_pWriteButton->EnableWindow (TRUE);
	m_pFocusCtrl = &m_PviPvarObj1;
}

void CPviDemoDlg::OnSetfocusPvar2() 
{
	// adjust idle state for variable 2:
	PviWriteRequest (m_PviPvarObj2.LinkID, POBJ_ACC_EVMASK, NULL, 0, NULL, 0, 0);

	// enable write button:
	m_pWriteButton->EnableWindow (TRUE);
	m_pFocusCtrl = &m_PviPvarObj2;
}

void CPviDemoDlg::OnSetfocusPvar3() 
{
	// adjust idle state for variable 3:
	PviWriteRequest (m_PviPvarObj3.LinkID, POBJ_ACC_EVMASK, NULL, 0, NULL, 0, 0);

	// enable write button:
	m_pWriteButton->EnableWindow (TRUE);
	m_pFocusCtrl = &m_PviPvarObj3;
}

void CPviDemoDlg::OnSetfocusPvar4() 
{
	// adjust idle state for variable 4:
	PviWriteRequest (m_PviPvarObj4.LinkID, POBJ_ACC_EVMASK, NULL, 0, NULL, 0, 0);

	// enable write button:
	m_pWriteButton->EnableWindow (TRUE);
	m_pFocusCtrl = &m_PviPvarObj4;
}

void CPviDemoDlg::OnKillfocusPvar1() 
{
	// get dialog input:
	GetDlgItemText (IDC_PVAR1, m_szWriteDataBf, PVI_STRDATA_MAXLEN + 1);

	if (m_bEventActive)
		// adjust active state for variable 1:
		PviWriteRequest (m_PviPvarObj1.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
}

void CPviDemoDlg::OnKillfocusPvar2() 
{
	// get dialog input:
	GetDlgItemText (IDC_PVAR2, m_szWriteDataBf, PVI_STRDATA_MAXLEN + 1);

	if (m_bEventActive)
		// adjust active state for variable 1:
		PviWriteRequest (m_PviPvarObj2.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
}

void CPviDemoDlg::OnKillfocusPvar3() 
{
	// get dialog input:
	GetDlgItemText (IDC_PVAR3, m_szWriteDataBf, PVI_STRDATA_MAXLEN + 1);

	if (m_bEventActive)
		// adjust active state for variable 1:
		PviWriteRequest (m_PviPvarObj3.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
}

void CPviDemoDlg::OnKillfocusPvar4() 
{
	// get dialog input:
	GetDlgItemText (IDC_PVAR4, m_szWriteDataBf, PVI_STRDATA_MAXLEN + 1);

	if (m_bEventActive)
		// adjust active state for variable 1:
		PviWriteRequest (m_PviPvarObj4.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
}

void CPviDemoDlg::OnActive() 
{
	//////////////////////////////////////////////////////////////////////////
	// The "Active" button was pressed in main dialog:

	// adjust active state on dialog:
	m_bEventActive = TRUE;
	m_pActiveButton->EnableWindow (m_bEventActive == FALSE);
	m_pIdleButton->EnableWindow (m_bEventActive != FALSE);

	// disable write button:
	m_pWriteButton->EnableWindow (FALSE);
	m_pFocusCtrl = NULL;

	// adjust active state for all created variable objects:
	if (m_PviPvarObj1.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj1.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
	if (m_PviPvarObj2.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj2.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
	if (m_PviPvarObj3.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj3.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
	if (m_PviPvarObj4.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj4.LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
}

void CPviDemoDlg::OnIdle() 
{
	//////////////////////////////////////////////////////////////////////////
	// The "Idle" button was pressed in main dialog:

	// adjust idle state on dialog:
	m_bEventActive = FALSE;
	m_pActiveButton->EnableWindow (m_bEventActive == FALSE);
	m_pIdleButton->EnableWindow (m_bEventActive != FALSE);
	
	// disable write button:
	m_pWriteButton->EnableWindow (FALSE);
	m_pFocusCtrl = NULL;

	// adjust idle state for all created variable objects:
	if (m_PviPvarObj1.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj1.LinkID, POBJ_ACC_EVMASK, "", 1, NULL, 0, 0);
	if (m_PviPvarObj2.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj2.LinkID, POBJ_ACC_EVMASK, "", 1, NULL, 0, 0);
	if (m_PviPvarObj3.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj3.LinkID, POBJ_ACC_EVMASK, "", 1, NULL, 0, 0);
	if (m_PviPvarObj4.LinkID != NULL)
		PviWriteRequest (m_PviPvarObj4.LinkID, POBJ_ACC_EVMASK, "", 1, NULL, 0, 0);
}

void CPviDemoDlg::OnWrite() 
{
	//////////////////////////////////////////////////////////////////////////
	// The "Write" button was pressed in main dialog:

	INT	ErrCode;

	if ((m_pFocusCtrl != NULL) && (m_pFocusCtrl->LinkID != NULL))
	{
		// write variable data:
		ErrCode = PviWriteRequest (m_pFocusCtrl->LinkID, POBJ_ACC_DATA,
								   m_szWriteDataBf, PVI_STRDATA_MAXLEN * sizeof (TCHAR),
								   CWnd::m_hWnd, WM_PVI_WRITE_RESP, (LPARAM) m_pFocusCtrl);
		if (ErrCode != 0)
			DisplayDemoMessage (_T("Fatal error on write request (%d)"), ErrCode);

		// disable write button:
		m_pWriteButton->EnableWindow (FALSE);
		m_pFocusCtrl = NULL;
	}
}		

LRESULT CPviDemoDlg::OnPviGlobalEvent (WPARAM wParam, LPARAM lParam)
{
	//////////////////////////////////////////////////////////////////////////
	// Pvi global event received:
	
	T_RESPONSE_INFO	Info;
	INT				ErrCode;

	// get event informations:
	ErrCode = PviGetResponseInfo (wParam, NULL, NULL, &Info, sizeof (Info));
	if (ErrCode != 0)
		return (0);
	
	// confirm global event message:
	ErrCode = PviReadResponse (wParam, NULL, 0);

	// interprete pvi global event:
	switch (Info.nType)
	{
		case POBJ_EVENT_PVI_CONNECT:	// connection to pvi Manager established
		{
			m_PviConnection = CONN_STATE_CONNECTED;

			// display connection state in dialog frame control:
			DisplayConnectionState ();
			break;
		}
		case POBJ_EVENT_PVI_DISCONN:	// connection to pvi Manager lost
		{
			m_PviConnection = CONN_STATE_DISCONNECTED;
			m_PviErrCode = ErrCode;
			m_CpuConnection = CONN_STATE_UNDEFINED;

			// display connection state in dialog frame control:
			DisplayConnectionState ();
			break;
		}
		case POBJ_EVENT_PVI_ARRANGE:	// demand to create pvi objects
		{
			CPviObjectsDlg ObjectsDlg;

			// load the pvi object parameter from the INI file:
			ObjectsDlg.LoadParam();

			// create new pvi Objects:
			CreateDemoObjects (ObjectsDlg);
		}
	}

	return (0);
}

LRESULT CPviDemoDlg::OnPviCpuEvent (WPARAM wParam, LPARAM lParam)
{
	//////////////////////////////////////////////////////////////////////////
	// Pvi event for cpu object received:

	INT	ErrCode;

	// confirm event message and read error or ready state:
	ErrCode = PviReadResponse (wParam, NULL, 0);

	if (ErrCode != 0)
	{
		m_CpuConnection = CONN_STATE_DISCONNECTED;
		m_CpuErrCode = ErrCode;
	}
	else
		m_CpuConnection = CONN_STATE_CONNECTED;

	// display connection state in dialog frame control:
	DisplayConnectionState ();

	return (0);
}

LRESULT CPviDemoDlg::OnPviPvarEvent (WPARAM wParam, LPARAM lParam)
{
	//////////////////////////////////////////////////////////////////////////
	// Pvi event for variable object received:

	T_PVIOBJ_CTRL*	pPviObjCtrl = (T_PVIOBJ_CTRL*) lParam;
	TCHAR			szReadDataBf[PVI_STRDATA_MAXLEN+1];
	INT				ErrCode;

	// confirm event message and read error or data:
	ErrCode = PviReadResponse (wParam, szReadDataBf, PVI_STRDATA_MAXLEN * sizeof (TCHAR));
	if (ErrCode != 0)
		_stprintf (szReadDataBf, _T("Error %d"), ErrCode);
	else
		szReadDataBf[PVI_STRDATA_MAXLEN] = _T('\0');	// -> force string termination

	if ((pPviObjCtrl != NULL) && (pPviObjCtrl != m_pFocusCtrl))
		SetDlgItemText (pPviObjCtrl->EditID, szReadDataBf);

	return (0);
}
	
LRESULT CPviDemoDlg::OnPviCreateResp (WPARAM wParam, LPARAM lParam)
{
	//////////////////////////////////////////////////////////////////////////
	// Pvi create response received:

	T_PVIOBJ_CTRL*	pPviObjCtrl = (T_PVIOBJ_CTRL*) lParam;
	INT				ErrCode;

	// confirm creation response message and analyse result:
	ErrCode = PviCreateResponse (wParam, &(pPviObjCtrl->LinkID));

	if (pPviObjCtrl->EditID != 0)
	{
		// pvi object has assigned dialog control:
		CEdit*	pEditCtrl = (CEdit*) GetDlgItem (pPviObjCtrl->EditID);
		CString	StateText;

		if (ErrCode != 0)
		{
			// create object error:
			StateText.Format (_T("Error %d"), ErrCode);
			pEditCtrl->SetWindowText (StateText);
		}
		else
		{
			// enable dialog control:
			pEditCtrl->EnableWindow (TRUE);
			pEditCtrl->SetReadOnly (FALSE);

			if (m_bEventActive)
				// adjust active state on created object:
				PviWriteRequest (pPviObjCtrl->LinkID, POBJ_ACC_EVMASK, "ed", 3, NULL, 0, 0);
		}
	}
	else
		// no assigned dialog control: 
		if (ErrCode != 0)
			DisplayDemoMessage (_T("Creation error on %s (%d)"), pPviObjCtrl->pNameID, ErrCode);

	return (0);
}

LRESULT CPviDemoDlg::OnPviWriteResp (WPARAM wParam, LPARAM lParam)
{
	//////////////////////////////////////////////////////////////////////////
	// Pvi variable data write response received:

	T_PVIOBJ_CTRL*	pPviObjCtrl = (T_PVIOBJ_CTRL*) lParam;
	INT				ErrCode;

	// confirm write response message and analyse write result:
	ErrCode = PviWriteResponse (wParam);
	if (ErrCode != 0)
		DisplayDemoMessage (_T("Write error on %s (%d)"), pPviObjCtrl->pNameID, ErrCode);

	return (0);
}
	
//////////////////////////////////////////////////////////////////////////////
