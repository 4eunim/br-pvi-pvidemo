
///////////////////////////////////////////////////////////////////////////////
// PviObjectsDlg.h: header file
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PVIOBJECTSDLG_H__B4D70FF1_DDB4_11D1_BB2C_008029B173CA__INCLUDED_)
#define AFX_PVIOBJECTSDLG_H__B4D70FF1_DDB4_11D1_BB2C_008029B173CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


/////////////////////////////////////////////////////////////////////////////
// CPviObjectsDlg dialog

class CPviObjectsDlg : public CDialog
{
// Construction
public:
	CPviObjectsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPviObjectsDlg)
	enum { IDD = IDD_PVIOBJECTS_DIALOG };
	CString	m_ConnectCpu;
	CString	m_ConnectDevice;
	CString	m_ConnectLine;
	CString	m_ConnectPv1;
	CString	m_ConnectPv2;
	CString	m_ConnectPv3;
	CString	m_ConnectPv4;
	CString	m_ConnectStation;
	CString	m_ConnectTask;
	CString	m_NameCpu;
	CString	m_NameDevice;
	CString	m_NameLine;
	CString	m_NamePv1;
	CString	m_NamePv2;
	CString	m_NamePv3;
	CString	m_NamePv4;
	CString	m_NameStation;
	CString	m_NameTask;
	CString	m_ParamPv1;
	CString	m_ParamPv2;
	CString	m_ParamPv3;
	CString	m_ParamPv4;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPviObjectsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPviObjectsDlg)
	virtual void OnOK();
	afx_msg void OnDefaultSerial();
	afx_msg void OnDefaultEthernet();
	afx_msg void OnDefaultARsim();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void SetDefaultSerial ();
	void SetDefaultEthernet ();
	void SetDefaultARsim ();

public:
	void LoadParam();
	void SaveParam();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PVIOBJECTSDLG_H__B4D70FF1_DDB4_11D1_BB2C_008029B173CA__INCLUDED_)
