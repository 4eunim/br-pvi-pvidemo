//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by PviDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PVIDEMO_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDD_PVICONNECT_DIALOG           129
#define IDD_PVIOBJECTS_DIALOG           130
#define IDC_STATIC_COMM                 1001
#define IDC_IDLE                        1002
#define IDC_ACTIVE                      1003
#define IDC_WRITE                       1004
#define IDC_PVAR1                       1005
#define IDC_PVAR2                       1006
#define IDC_PVAR3                       1007
#define IDC_PVAR4                       1008
#define IDC_CONNECT                     1009
#define IDC_OBJECTS                     1010
#define IDC_COMM_LOCAL                  1011
#define IDC_COMM_REMOTE                 1012
#define IDC_COMM_IPADDR                 1013
#define IDC_COMM_PORT                   1014
#define IDC_COMM_TIMEOUT                1015
#define IDC_DEFAULT_CONN                1016
#define IDC_NAME_LINE                   1017
#define IDC_NAME_DEVICE                 1018
#define IDC_NAME_STATION                1019
#define IDC_NAME_CPU                    1020
#define IDC_NAME_TASK                   1021
#define IDC_NAME_PV1                    1022
#define IDC_NAME_PV2                    1023
#define IDC_NAME_PV3                    1024
#define IDC_NAME_PV4                    1025
#define IDC_CONNECT_LINE                1026
#define IDC_CONNECT_DEVICE              1027
#define IDC_CONNECT_STATION             1028
#define IDC_CONNECT_CPU                 1029
#define IDC_CONNECT_TASK                1030
#define IDC_CONNECT_PV1                 1031
#define IDC_CONNECT_PV2                 1032
#define IDC_CONNECT_PV3                 1033
#define IDC_CONNECT_PV4                 1034
#define IDC_PVI_PARAM_PV1               1035
#define IDC_PVI_PARAM_PV2               1036
#define IDC_PVI_PARAM_PV3               1037
#define IDC_PVI_PARAM_PV4               1038
#define IDC_DEFAULT_SERIAL              1039
#define IDC_DEFAULT_ETHERNET            1040
#define IDC_DEFAULT_ARSIM               1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
