
///////////////////////////////////////////////////////////////////////////////
// PviConnectDlg.h: header file
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PVICONNECTDLG_H__AD630E61_8B8F_11D2_9034_444553540000__INCLUDED_)
#define AFX_PVICONNECTDLG_H__AD630E61_8B8F_11D2_9034_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CPviConnectDlg dialog

class CPviConnectDlg : public CDialog
{
// Construction
public:
	CPviConnectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPviConnectDlg)
	enum { IDD = IDD_PVICONNECT_DIALOG };
	CString	m_IpAddr;
	UINT	m_Port;
	UINT	m_CommTimeout;
	int		m_CommRemote;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPviConnectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPviConnectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnCommLocal();
	afx_msg void OnCommRemote();
	afx_msg void OnCommDefault();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void SetCommDefault ();

public:
	void LoadParam();
	void SaveParam();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PVICONNECTDLG_H__AD630E61_8B8F_11D2_9034_444553540000__INCLUDED_)
