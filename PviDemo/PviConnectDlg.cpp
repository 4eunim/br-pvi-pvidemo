
///////////////////////////////////////////////////////////////////////////////
// PviConnectDlg.cpp: PVI connection specification dialog class
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PviDemo.h"
#include "PviConnectDlg.h"

#ifdef UNDER_CE					// Windows CE only
#include "Profile.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CPviDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPviConnectDlg dialog

CPviConnectDlg::CPviConnectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPviConnectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPviConnectDlg)
	m_IpAddr = _T("");
	m_Port = 0;
	m_CommTimeout = 0;
	m_CommRemote = -1;
	//}}AFX_DATA_INIT
}

void CPviConnectDlg::SetCommDefault() 
{
	// Default PVI communication parameters:
	m_CommRemote	= FALSE;
	m_IpAddr		= _T("127.0.0.1");
	m_Port			= 20000;
	m_CommTimeout	= 10;
}

void CPviConnectDlg::LoadParam ()
{
	LPCTSTR	pszAppName = theApp.m_pszAppName;
	TCHAR	ProfileName[1024];
	TCHAR	StrBf[512];
	TCHAR*	pStr;
	CString	ValueString;

	// Build pathname to INI file:
	GetModuleFileName (NULL, ProfileName, (sizeof (ProfileName) / sizeof (TCHAR)) - 16);
	if ((pStr = _tcsrchr (ProfileName, _T('\\'))) == NULL)
		pStr = ProfileName;
	else
		pStr++;
	_tcscpy (pStr, theApp.m_pszProfileName);

	// set to default parameter:
	SetCommDefault ();

	// read PVI communication parameter:
	ValueString.Format (_T("%u"), m_CommRemote);
	GetPrivateProfileString (pszAppName, _T("SockUse"), ValueString,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_CommRemote = (_tcstol (StrBf, NULL, 10) != 0);
	GetPrivateProfileString (pszAppName, _T("SockAddr"), m_IpAddr,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_IpAddr = StrBf;
	ValueString.Format (_T("%u"), m_Port);
	GetPrivateProfileString (pszAppName, _T("SockPort"), ValueString,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_Port = _tcstol (StrBf, NULL, 10);
	ValueString.Format (_T("%u"), m_CommTimeout);
	GetPrivateProfileString (pszAppName, _T("CommTimeout"), ValueString,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_CommTimeout = _tcstol (StrBf, NULL, 10);
}

void CPviConnectDlg::SaveParam ()
{
	LPCTSTR	pszAppName = theApp.m_pszAppName;
	TCHAR	ProfileName[1024];
	TCHAR*	pStr;
	CString	ValueString;

	// Build pathname to INI file:
	GetModuleFileName (NULL, ProfileName, (sizeof (ProfileName) / sizeof (TCHAR)) - 16);
	if ((pStr = _tcsrchr (ProfileName, _T('\\'))) == NULL)
		pStr = ProfileName;
	else
		pStr++;
	_tcscpy (pStr, theApp.m_pszProfileName);

	// write PVI names to INI file:
	ValueString.Format (_T("%u"), m_CommRemote);
	WritePrivateProfileString (pszAppName, _T("SockUse"), ValueString,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("SockAddr"), m_IpAddr,
							   ProfileName);
	ValueString.Format (_T("%u"), m_Port);
	WritePrivateProfileString (pszAppName, _T("SockPort"), ValueString,
							   ProfileName);
	ValueString.Format (_T("%u"), m_CommTimeout);
	WritePrivateProfileString (pszAppName, _T("CommTimeout"), ValueString,
							   ProfileName);
}

void CPviConnectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPviConnectDlg)
	DDX_Text(pDX, IDC_COMM_IPADDR, m_IpAddr);
	DDX_Text(pDX, IDC_COMM_PORT, m_Port);
	DDX_Text(pDX, IDC_COMM_TIMEOUT, m_CommTimeout);
	DDX_Radio(pDX, IDC_COMM_LOCAL, m_CommRemote);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPviConnectDlg, CDialog)
	//{{AFX_MSG_MAP(CPviConnectDlg)
	ON_BN_CLICKED(IDC_COMM_LOCAL, OnCommLocal)
	ON_BN_CLICKED(IDC_COMM_REMOTE, OnCommRemote)
	ON_BN_CLICKED(IDC_DEFAULT_CONN, OnCommDefault)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPviConnectDlg message handlers

BOOL CPviConnectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (m_CommRemote)
		OnCommRemote ();
	else
		OnCommLocal ();
	
	return TRUE;
}

void CPviConnectDlg::OnCommLocal() 
{
	// selected local communication:
	CWnd*	pCtrlIpAddr = GetDlgItem (IDC_COMM_IPADDR);
	CWnd*	pCtrlPort = GetDlgItem (IDC_COMM_PORT);

	pCtrlIpAddr->EnableWindow (FALSE);		
	pCtrlPort->EnableWindow (FALSE);		
}

void CPviConnectDlg::OnCommRemote() 
{
	// selected remote (TCP/IP) communication:
	CWnd*	pCtrlIpAddr = GetDlgItem (IDC_COMM_IPADDR);
	CWnd*	pCtrlPort = GetDlgItem (IDC_COMM_PORT);

	pCtrlIpAddr->EnableWindow (TRUE);		
	pCtrlPort->EnableWindow (TRUE);		
}

void CPviConnectDlg::OnCommDefault() 
{
	SetCommDefault ();
	if (m_CommRemote)
		OnCommRemote ();
	else
		OnCommLocal ();
	UpdateData (FALSE);
}

//////////////////////////////////////////////////////////////////////////////

