
/******************************************************************************
* Emulate win32 INI file functions (required for windows CE)
******************************************************************************/

#include "stdafx.h"


// The emulation saves and respores INI file entries in windows registry.
// Registry path name:
//	BR_REG_DIRECTORY \ <ini filename> \ <ini section> \ <ini entry 1..x>
//    <ini filename ... file name part of argument pFileName 
//    <ini section> ... argument pAppName 
//    <ini entry>   ... argument pKeyName

#define BR_REG_DIRECTORY 	"SOFTWARE\\BR_Automation"
#define BR_REG_PATH_MAXLEN 	1024


/*=============================================================================
* Function GetPrivateProfileString:
* retrieves a string from the specified section in an initialization file.
*============================================================================*/

DWORD GetPrivateProfileString (LPCTSTR	pAppName,
							   LPCTSTR	pKeyName,
							   LPCTSTR	pDefault,
							   LPTSTR	pReturnedString,
							   DWORD	nSize,
							   LPCTSTR	pFileName)
{
	long		Result;
	HKEY		hKey;
	TCHAR		PathName[BR_REG_PATH_MAXLEN];
	TCHAR*		pName;
	DWORD		Len;
	BYTE		EntryBf[2048];
	DWORD		EntryLen;
	DWORD		ValueType;

	if ((pAppName == NULL) ||
		(pKeyName == NULL) ||
		(nSize == 0))
	{
		pReturnedString[0] = _T('\0');
		return (0);
	}

	PathName[BR_REG_PATH_MAXLEN - 1] = _T('\0');
	_tcscpy (PathName, _T(BR_REG_DIRECTORY));
	if ((pName = _tcsrchr (pFileName, _T('\\'))) == NULL)
		pName = (TCHAR*) pFileName;
	else
		pName++;
	Len = _tcslen (PathName);
	PathName[Len++] = _T('\\');
	_tcsncpy (PathName + Len, pName, BR_REG_PATH_MAXLEN - Len - 2);
	Len = _tcslen (PathName);
	PathName[Len++] = _T('\\');
	_tcsncpy (PathName + Len, pAppName, BR_REG_PATH_MAXLEN - Len - 1);

	Result = RegOpenKeyEx (HKEY_LOCAL_MACHINE, PathName, 0, KEY_READ, &hKey);
	if (Result == ERROR_SUCCESS)
	{
		EntryLen = sizeof (EntryBf);
		Result = RegQueryValueEx (hKey, pKeyName, NULL,
								  &ValueType, (BYTE*) EntryBf, &EntryLen);
		if (Result == ERROR_SUCCESS)
			if (EntryLen == 0)
				pReturnedString[0] = _T('\0');
			else
			{
				if (EntryLen > nSize)  
					EntryLen = nSize;
				memcpy (pReturnedString, EntryBf, EntryLen);
				pReturnedString[EntryLen - 1] = _T('\0');
			}

		RegCloseKey (hKey);
	}
	if (Result != ERROR_SUCCESS)
		if (pDefault == NULL)
			pReturnedString[0] = _T('\0');
		else
		{
			_tcsncpy (pReturnedString, pDefault, nSize);
			pReturnedString[nSize - 1] = _T('\0');
		}

	return (_tcslen (pReturnedString));
}


/*=============================================================================
* Function WritePrivateProfileString:
* copies a string into the specified section of the specified file.
*============================================================================*/

BOOL WritePrivateProfileString (LPCTSTR	pAppName,
							    LPCTSTR	pKeyName,
							    LPCTSTR	pString, 
							    LPCTSTR	pFileName)
{
	long		Result;
	HKEY		hKey;
	TCHAR		PathName[BR_REG_PATH_MAXLEN];
	TCHAR*		pName;
	DWORD		Len;
	DWORD		Disposition;

	if ((pAppName == NULL) ||
		(pKeyName == NULL))
		return (TRUE);

	PathName[BR_REG_PATH_MAXLEN - 1] = _T('\0');
	_tcscpy (PathName, _T(BR_REG_DIRECTORY));
	if ((pName = _tcsrchr (pFileName, _T('\\'))) == NULL)
		pName = (TCHAR*) pFileName;
	else
		pName++;
	Len = _tcslen (PathName);
	PathName[Len++] = _T('\\');
	_tcsncpy (PathName + Len, pName, BR_REG_PATH_MAXLEN - Len - 2);
	Len = _tcslen (PathName);
	PathName[Len++] = _T('\\');
	_tcsncpy (PathName + Len, pAppName, BR_REG_PATH_MAXLEN - Len - 1);

	Result = RegCreateKeyEx (HKEY_LOCAL_MACHINE, PathName,
							 0, (TCHAR*) pKeyName, REG_OPTION_NON_VOLATILE,
							 KEY_WRITE, NULL, &hKey, &Disposition);
	if (Result == ERROR_SUCCESS)
	{
		if (pString == NULL)
			pString = _T("");
		Result = RegSetValueEx (hKey, pKeyName, 0, REG_SZ, (BYTE*) pString,
								(_tcslen (pString) + 1) * sizeof (TCHAR));

		RegCloseKey (hKey);
	}
	return (Result == ERROR_SUCCESS);
}

	
