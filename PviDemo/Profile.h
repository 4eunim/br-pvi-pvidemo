
/******************************************************************************
* Emulate win32 INI file functions (required for windows CE)
******************************************************************************/

extern DWORD GetPrivateProfileString (LPCTSTR	pAppName,
									  LPCTSTR	pKeyName,
									  LPCTSTR	pDefault,
									  LPTSTR	pReturnedString,
									  DWORD		nSize,
									  LPCTSTR	pFileName);

extern BOOL WritePrivateProfileString (LPCTSTR	pAppName,
									   LPCTSTR	pKeyName,
									   LPCTSTR	pString, 
									   LPCTSTR	pFileName);


