
///////////////////////////////////////////////////////////////////////////////
// PviDemo.h: main header file
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PVIDEMO_H__B4D70FE7_DDB4_11D1_BB2C_008029B173CA__INCLUDED_)
#define AFX_PVIDEMO_H__B4D70FE7_DDB4_11D1_BB2C_008029B173CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

// PVI messages:
#define WM_PVI_GLOBAL_EVENT	(WM_USER+1)
#define WM_PVI_CPU_EVENT	(WM_USER+2)
#define WM_PVI_PVAR_EVENT	(WM_USER+3)
#define WM_PVI_CREATE_RESP	(WM_USER+4)
#define WM_PVI_WRITE_RESP	(WM_USER+5)


/////////////////////////////////////////////////////////////////////////////
// CPviDemoApp:
// See PviDemo.cpp for the implementation of this class
//

class CPviDemoApp : public CWinApp
{
public:
	CPviDemoApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPviDemoApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPviDemoApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// CExString:
// Extended CString class to handle UNICODE conversions
//

class CExString : public CString
{
public:
	CExString();
	~CExString();

	using CString::operator=;

#ifdef UNICODE
   // return pointer to const ansi character string
    operator LPCSTR()
	{
		return (GetCharString());
	};
#endif

protected:
	LPSTR m_szCharString;

	LPCSTR GetCharString();
};


/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PVIDEMO_H__B4D70FE7_DDB4_11D1_BB2C_008029B173CA__INCLUDED_)
