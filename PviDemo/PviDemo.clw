; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "pvidemo.h"
LastPage=0

ClassCount=5
Class1=CPviConnectDlg
Class2=CPviDemoApp
Class3=CAboutDlg
Class4=CPviDemoDlg
Class5=CPviObjectsDlg

ResourceCount=4
Resource1=IDD_PVIDEMO_DIALOG (Neutral)
Resource2=IDD_ABOUTBOX (Neutral)
Resource3=IDD_PVIOBJECTS_DIALOG (Neutral)
Resource4=IDD_PVICONNECT_DIALOG (Neutral)

[CLS:CPviConnectDlg]
Type=0
BaseClass=CDialog
HeaderFile=PviConnectDlg.h
ImplementationFile=PviConnectDlg.cpp
LastObject=CPviConnectDlg

[CLS:CPviDemoApp]
Type=0
BaseClass=CWinApp
HeaderFile=PviDemo.h
ImplementationFile=PviDemo.cpp
LastObject=CPviDemoApp
Filter=N

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=PviDemoDlg.cpp
ImplementationFile=PviDemoDlg.cpp
LastObject=CAboutDlg
Filter=D

[CLS:CPviDemoDlg]
Type=0
BaseClass=CDialog
HeaderFile=PviDemoDlg.h
ImplementationFile=PviDemoDlg.cpp
LastObject=CPviDemoDlg

[CLS:CPviObjectsDlg]
Type=0
BaseClass=CDialog
HeaderFile=PviObjectsDlg.h
ImplementationFile=PviObjectsDlg.cpp
LastObject=CPviObjectsDlg

[DLG:IDD_PVICONNECT_DIALOG]
Type=1
Class=CPviConnectDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg

[DLG:IDD_PVIDEMO_DIALOG]
Type=1
Class=CPviDemoDlg

[DLG:IDD_PVIOBJECTS_DIALOG]
Type=1
Class=CPviObjectsDlg

[DLG:IDD_ABOUTBOX (Neutral)]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_PVIDEMO_DIALOG (Neutral)]
Type=1
Class=?
ControlCount=15
Control1=IDCANCEL,button,1342242816
Control2=IDC_PVAR1,edit,1350631552
Control3=IDC_PVAR2,edit,1350631552
Control4=IDC_PVAR3,edit,1350631552
Control5=IDC_PVAR4,edit,1350631552
Control6=IDC_ACTIVE,button,1342242816
Control7=IDC_IDLE,button,1342242816
Control8=IDC_WRITE,button,1342242816
Control9=IDC_CONNECT,button,1342242816
Control10=IDC_OBJECTS,button,1342242816
Control11=IDC_STATIC,static,1342177794
Control12=IDC_STATIC,static,1342177794
Control13=IDC_STATIC,static,1342177794
Control14=IDC_STATIC,static,1342177794
Control15=IDC_STATIC_COMM,button,1342210055

[DLG:IDD_PVIOBJECTS_DIALOG (Neutral)]
Type=1
Class=?
ControlCount=39
Control1=IDC_NAME_LINE,edit,1350762624
Control2=IDC_CONNECT_LINE,edit,1350631552
Control3=IDC_NAME_DEVICE,edit,1350762624
Control4=IDC_CONNECT_DEVICE,edit,1350631552
Control5=IDC_NAME_STATION,edit,1350762624
Control6=IDC_CONNECT_STATION,edit,1350631552
Control7=IDC_NAME_CPU,edit,1350762624
Control8=IDC_CONNECT_CPU,edit,1350631552
Control9=IDC_NAME_TASK,edit,1350762624
Control10=IDC_CONNECT_TASK,edit,1350631552
Control11=IDC_NAME_PV1,edit,1350762624
Control12=IDC_CONNECT_PV1,edit,1350631552
Control13=IDC_PVI_PARAM_PV1,edit,1350631552
Control14=IDC_NAME_PV2,edit,1350762624
Control15=IDC_CONNECT_PV2,edit,1350631552
Control16=IDC_PVI_PARAM_PV2,edit,1350631552
Control17=IDC_NAME_PV3,edit,1350762624
Control18=IDC_CONNECT_PV3,edit,1350631552
Control19=IDC_PVI_PARAM_PV3,edit,1350631552
Control20=IDC_NAME_PV4,edit,1350762624
Control21=IDC_CONNECT_PV4,edit,1350631552
Control22=IDC_PVI_PARAM_PV4,edit,1350631552
Control23=IDC_DEFAULT_SERIAL,button,1342242816
Control24=IDC_DEFAULT_ETHERNET,button,1342242816
Control25=IDOK,button,1342242817
Control26=IDCANCEL,button,1342242816
Control27=IDC_STATIC,static,1342308866
Control28=IDC_STATIC,static,1342308866
Control29=IDC_STATIC,static,1342308866
Control30=IDC_STATIC,static,1342308866
Control31=IDC_STATIC,static,1342308866
Control32=IDC_STATIC,static,1342308866
Control33=IDC_STATIC,static,1342308866
Control34=IDC_STATIC,static,1342308866
Control35=IDC_STATIC,static,1342308864
Control36=IDC_STATIC,static,1342308864
Control37=IDC_STATIC,static,1342308864
Control38=IDC_STATIC,static,1342308866
Control39=IDC_DEFAULT_ARSIM,button,1342242816

[DLG:IDD_PVICONNECT_DIALOG (Neutral)]
Type=1
Class=?
ControlCount=11
Control1=IDC_COMM_LOCAL,button,1342373897
Control2=IDC_COMM_REMOTE,button,1342177289
Control3=IDC_COMM_IPADDR,edit,1350762624
Control4=IDC_COMM_PORT,edit,1350631552
Control5=IDC_COMM_TIMEOUT,edit,1350631552
Control6=IDOK,button,1342242817
Control7=IDCANCEL,button,1342242816
Control8=IDC_STATIC,static,1342177794
Control9=IDC_STATIC,static,1342308866
Control10=IDC_STATIC,static,1342308866
Control11=IDC_DEFAULT_CONN,button,1342242817

