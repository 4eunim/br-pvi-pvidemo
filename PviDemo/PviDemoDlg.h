
///////////////////////////////////////////////////////////////////////////////
// PviDemoDlg.h: header file
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PVIDEMODLG_H__B4D70FE9_DDB4_11D1_BB2C_008029B173CA__INCLUDED_)
#define AFX_PVIDEMODLG_H__B4D70FE9_DDB4_11D1_BB2C_008029B173CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


//////////////////////////////////////////////////////////////////////////////
// T_PVIOBJ_CTRL structure for PVI object controlling

typedef struct t_pviobj_ctrl
{
	const TCHAR*	pNameID;	// name identifier 
	int				EditID;		// dialog edit control identifier
	DWORD			LinkID;		// PVI link object identifier (handle)
}	T_PVIOBJ_CTRL;


/////////////////////////////////////////////////////////////////////////////
// CPviDemoDlg dialog

typedef enum t_conn_state
{
	CONN_STATE_UNDEFINED,		// undefined state
	CONN_STATE_DISCONNECTED,	// disconnected state
	CONN_STATE_CONNECTED,		// connected state
}	T_CONN_STATE;

#define PVI_STRDATA_MAXLEN		80


class CPviDemoDlg : public CDialog
{
// Construction
public:
	CPviDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPviDemoDlg)
	enum { IDD = IDD_PVIDEMO_DIALOG };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPviDemoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	// PVI object controlling:
	T_PVIOBJ_CTRL	m_PviLineObj;
	T_PVIOBJ_CTRL	m_PviDeviceObj;
	T_PVIOBJ_CTRL	m_PviStationObj;
	T_PVIOBJ_CTRL	m_PviCpuObj;
	T_PVIOBJ_CTRL	m_PviTaskObj;
	T_PVIOBJ_CTRL	m_PviPvarObj1;
	T_PVIOBJ_CTRL	m_PviPvarObj2;
	T_PVIOBJ_CTRL	m_PviPvarObj3;
	T_PVIOBJ_CTRL	m_PviPvarObj4;

	// PVI Demo Functions:
	void DisplayDemoMessage (LPCTSTR pText, ...);
	void DisplayConnectionState ();
	void DisableDemoObjectControls ();
	void CreateDemoObjects (CPviObjectsDlg&	Objects);
	void FreeDemoObjects ();
	void CreateConnection (CPviConnectDlg& Connect);
	void FreeConnection ();

protected:
	// current connection state:
	T_CONN_STATE	m_PviConnection;	// PVI manager connection
	INT				m_PviErrCode;
	T_CONN_STATE	m_CpuConnection;	// CPU object connection
	INT				m_CpuErrCode;

	// dialog controlling:
	HICON			m_hIcon;
	bool			m_bEventActive;
	CButton*		m_pActiveButton;
	CButton*		m_pIdleButton;
	CButton*		m_pWriteButton;
	TCHAR			m_szWriteDataBf[PVI_STRDATA_MAXLEN+1];
	T_PVIOBJ_CTRL*	m_pFocusCtrl;

	// Generated message map functions
	//{{AFX_MSG(CPviDemoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnConnect();
	afx_msg void OnObjects();
	afx_msg void OnSetfocusPvar1();
	afx_msg void OnSetfocusPvar2();
	afx_msg void OnSetfocusPvar3();
	afx_msg void OnSetfocusPvar4();
	afx_msg void OnKillfocusPvar1();
	afx_msg void OnKillfocusPvar2();
	afx_msg void OnKillfocusPvar3();
	afx_msg void OnKillfocusPvar4();
	afx_msg void OnActive();
	afx_msg void OnIdle();
	afx_msg void OnWrite();
	//}}AFX_MSG

	// PVI Message Functions:
	LRESULT OnPviGlobalEvent (WPARAM, LPARAM);
	LRESULT OnPviCpuEvent (WPARAM, LPARAM);
	LRESULT OnPviPvarEvent (WPARAM, LPARAM);
	LRESULT OnPviCreateResp (WPARAM, LPARAM);
	LRESULT OnPviWriteResp (WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PVIDEMODLG_H__B4D70FE9_DDB4_11D1_BB2C_008029B173CA__INCLUDED_)
