
///////////////////////////////////////////////////////////////////////////////
// PviDemo.cpp: Application class
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PviDemo.h"
#include "PviConnectDlg.h"
#include "PviObjectsDlg.h"
#include "PviDemoDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// The one and only CPviDemoApp object

CPviDemoApp theApp;


/////////////////////////////////////////////////////////////////////////////
// CPviDemoApp

BEGIN_MESSAGE_MAP(CPviDemoApp, CWinApp)
	//{{AFX_MSG_MAP(CPviDemoApp)
	//}}AFX_MSG
	// ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPviDemoApp construction

CPviDemoApp::CPviDemoApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// CPviDemoApp initialization

BOOL CPviDemoApp::InitInstance()
{
	// Standard initialization
#ifdef UNDER_CE
	SetRegistryKey (_T("BrAutomation"));
#else
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif	// _AFXDLL
#endif	// UNDER_CE

	CPviDemoDlg dlg;
	CString		ProfileName;

	if (m_pszProfileName == NULL)
	{
		// if profile ist undefined:
		ProfileName = m_pszAppName;
		ProfileName += _T(".ini");
		m_pszProfileName = ProfileName;
	}

	m_pMainWnd = &dlg;

	dlg.DoModal();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CExString:
// Extended CString class to handle UNICODE conversions
//

CExString::CExString()
{
	m_szCharString = NULL;
}

CExString::~CExString()
{
	if (m_szCharString != NULL)
		delete m_szCharString;
}

LPCSTR CExString::GetCharString()
{
	int				Len;
	const TCHAR*	pSrc;
	char*			pDst;

	Len = CString::GetLength ();
	if (m_szCharString != NULL)
		delete m_szCharString;
	m_szCharString = new char[Len + 1];
	if (m_szCharString == NULL)
		return ("OOPS");

	pSrc = *(this);
	pDst = m_szCharString;
	while (Len-- > 0)
		*(pDst++) = (char) *(pSrc++);
	*pDst = '\0';

	return (m_szCharString);
}
