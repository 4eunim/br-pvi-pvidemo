
///////////////////////////////////////////////////////////////////////////////
// PviObjectsDlg.cpp: PVI object specification dialog class
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PviDemo.h"
#include "PviObjectsDlg.h"

#ifdef UNDER_CE					// Windows CE only
#include "Profile.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CPviDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPviObjectsDlg dialog

CPviObjectsDlg::CPviObjectsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPviObjectsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPviObjectsDlg)
	m_ConnectCpu = _T("");
	m_ConnectDevice = _T("");
	m_ConnectLine = _T("");
	m_ConnectPv1 = _T("");
	m_ConnectPv2 = _T("");
	m_ConnectPv3 = _T("");
	m_ConnectPv4 = _T("");
	m_ConnectStation = _T("");
	m_ConnectTask = _T("");
	m_NameCpu = _T("");
	m_NameDevice = _T("");
	m_NameLine = _T("");
	m_NamePv1 = _T("");
	m_NamePv2 = _T("");
	m_NamePv3 = _T("");
	m_NamePv4 = _T("");
	m_NameStation = _T("");
	m_NameTask = _T("");
	m_ParamPv1 = _T("");
	m_ParamPv2 = _T("");
	m_ParamPv3 = _T("");
	m_ParamPv4 = _T("");
	//}}AFX_DATA_INIT
}

void CPviObjectsDlg::SetDefaultSerial() 
{
	// Default PVI names:
	m_NameLine		 = _T("PviDemo");
	m_NameDevice	 = _T("Serial");
	m_NameStation	 = _T("");
	m_NameCpu		 = _T("Cpu");

	// Default connection strings: 
	m_ConnectLine	 = _T("LnIna2");
	m_ConnectDevice  = _T("/IF=com1 /BD=57600 /PA=2");
	m_ConnectStation = _T("");
	m_ConnectCpu	 = _T("/RT=1500");
}

void CPviObjectsDlg::SetDefaultEthernet() 
{
	// Default PVI names:
	m_NameLine		 = _T("PviDemo");
	m_NameDevice	 = _T("Ethernet");
	m_NameStation	 = _T("");
	m_NameCpu		 = _T("Cpu");

	// Default connection strings: 
	m_ConnectLine	 = _T("LnIna2");
	m_ConnectDevice  = _T("/IF=tcpip /SA=1");
	m_ConnectStation = _T("");
	m_ConnectCpu	 = _T("/DAIP=10.0.0.2 /RT=500");
}

void CPviObjectsDlg::SetDefaultARsim() 
{
	// Default PVI names:
	m_NameLine		 = _T("PviDemo");
	m_NameDevice	 = _T("ARsim");
	m_NameStation	 = _T("");
	m_NameCpu		 = _T("Cpu");

	// Default connection strings: 
	m_ConnectLine	 = _T("LnIna2");
	m_ConnectDevice  = _T("/IF=tcpip /SA=1");
	m_ConnectStation = _T("");
	m_ConnectCpu	 = _T("/DAIP=127.0.0.1 /REPO=11160 /RT=1000");
}

void CPviObjectsDlg::LoadParam ()
{
	LPCTSTR	pszAppName = theApp.m_pszAppName;
	TCHAR	ProfileName[1024];
	TCHAR	StrBf[512];
	TCHAR*	pStr;

	// Build pathname to INI file:
	GetModuleFileName (NULL, ProfileName, (sizeof (ProfileName) / sizeof (TCHAR)) - 16);
	if ((pStr = _tcsrchr (ProfileName, _T('\\'))) == NULL)
		pStr = ProfileName;
	else
		pStr++;
	_tcscpy (pStr, theApp.m_pszProfileName);

	// set to default parameter:
	SetDefaultEthernet ();

	// read PVI names from INI file:
	GetPrivateProfileString (pszAppName, _T("NameLine"), m_NameLine,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NameLine = StrBf;
	GetPrivateProfileString (pszAppName, _T("NameDevice"), m_NameDevice,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NameDevice = StrBf;
	GetPrivateProfileString (pszAppName, _T("NameStation"), m_NameStation,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NameStation = StrBf;
	GetPrivateProfileString (pszAppName, _T("NameCpu"), m_NameCpu,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NameCpu = StrBf;
	GetPrivateProfileString (pszAppName, _T("NameTask"), m_NameTask,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NameTask = StrBf;
	GetPrivateProfileString (pszAppName, _T("NamePv1"), m_NamePv1,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NamePv1 = StrBf;
	GetPrivateProfileString (pszAppName, _T("NamePv2"), m_NamePv2,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NamePv2 = StrBf;
	GetPrivateProfileString (pszAppName, _T("NamePv3"), m_NamePv3,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NamePv3 = StrBf;
	GetPrivateProfileString (pszAppName, _T("NamePv4"), m_NamePv4,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_NamePv4 = StrBf;

	// read PVI connection strings from INI file:
	GetPrivateProfileString (pszAppName, _T("ConnectLine"), m_ConnectLine,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectLine = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectDevice"), m_ConnectDevice,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectDevice = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectStation"), m_ConnectStation,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectStation = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectCpu"), m_ConnectCpu,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectCpu = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectTask"), m_ConnectTask,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectTask = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectPv1"), m_ConnectPv1,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectPv1 = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectPv2"), m_ConnectPv2,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectPv2 = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectPv3"), m_ConnectPv3,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectPv3 = StrBf;
	GetPrivateProfileString (pszAppName, _T("ConnectPv4"), m_ConnectPv4,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ConnectPv4 = StrBf;

	// read PVI parameter strings from INI file:
	GetPrivateProfileString (pszAppName, _T("ParamPv1"), m_ParamPv1,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ParamPv1 = StrBf;
	GetPrivateProfileString (pszAppName, _T("ParamPv2"), m_ParamPv2,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ParamPv2 = StrBf;
	GetPrivateProfileString (pszAppName, _T("ParamPv3"), m_ParamPv3,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ParamPv3 = StrBf;
	GetPrivateProfileString (pszAppName, _T("ParamPv4"), m_ParamPv4,
							 StrBf, sizeof (StrBf) / sizeof (TCHAR), ProfileName);
	m_ParamPv4 = StrBf;
}

void CPviObjectsDlg::SaveParam ()
{
	LPCTSTR	pszAppName = theApp.m_pszAppName;
	TCHAR	ProfileName[1024];
	TCHAR*	pStr;

	// Build pathname to INI file:
	GetModuleFileName (NULL, ProfileName, (sizeof (ProfileName) / sizeof (TCHAR)) - 16);
	if ((pStr = _tcsrchr (ProfileName, _T('\\'))) == NULL)
		pStr = ProfileName;
	else
		pStr++;
	_tcscpy (pStr, theApp.m_pszProfileName);

	// write PVI names to INI file:
	WritePrivateProfileString (pszAppName, _T("NameLine"), m_NameLine,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NameDevice"), m_NameDevice,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NameStation"), m_NameStation,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NameCpu"), m_NameCpu,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NameTask"), m_NameTask,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NamePv1"), m_NamePv1,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NamePv2"), m_NamePv2,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NamePv3"), m_NamePv3,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("NamePv4"), m_NamePv4,
							   ProfileName);

	// write PVI connection strings to INI file:
	WritePrivateProfileString (pszAppName, _T("ConnectLine"), m_ConnectLine,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectDevice"), m_ConnectDevice,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectStation"), m_ConnectStation,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectCpu"), m_ConnectCpu,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectTask"), m_ConnectTask,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectPv1"), m_ConnectPv1,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectPv2"), m_ConnectPv2,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectPv3"), m_ConnectPv3,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ConnectPv4"), m_ConnectPv4,
							   ProfileName);

	// write PVI data format strings to INI file:
	WritePrivateProfileString (pszAppName, _T("ParamPv1"), m_ParamPv1,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ParamPv2"), m_ParamPv2,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ParamPv3"), m_ParamPv3,
							   ProfileName);
	WritePrivateProfileString (pszAppName, _T("ParamPv4"), m_ParamPv4,
							   ProfileName);
}

void CPviObjectsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPviObjectsDlg)
	DDX_Text(pDX, IDC_CONNECT_CPU, m_ConnectCpu);
	DDV_MaxChars(pDX, m_ConnectCpu, 512);
	DDX_Text(pDX, IDC_CONNECT_DEVICE, m_ConnectDevice);
	DDV_MaxChars(pDX, m_ConnectDevice, 512);
	DDX_Text(pDX, IDC_CONNECT_LINE, m_ConnectLine);
	DDV_MaxChars(pDX, m_ConnectLine, 512);
	DDX_Text(pDX, IDC_CONNECT_PV1, m_ConnectPv1);
	DDV_MaxChars(pDX, m_ConnectPv1, 512);
	DDX_Text(pDX, IDC_CONNECT_PV2, m_ConnectPv2);
	DDV_MaxChars(pDX, m_ConnectPv2, 512);
	DDX_Text(pDX, IDC_CONNECT_PV3, m_ConnectPv3);
	DDV_MaxChars(pDX, m_ConnectPv3, 512);
	DDX_Text(pDX, IDC_CONNECT_PV4, m_ConnectPv4);
	DDV_MaxChars(pDX, m_ConnectPv4, 512);
	DDX_Text(pDX, IDC_CONNECT_STATION, m_ConnectStation);
	DDV_MaxChars(pDX, m_ConnectStation, 512);
	DDX_Text(pDX, IDC_CONNECT_TASK, m_ConnectTask);
	DDV_MaxChars(pDX, m_ConnectTask, 512);
	DDX_Text(pDX, IDC_NAME_CPU, m_NameCpu);
	DDV_MaxChars(pDX, m_NameCpu, 256);
	DDX_Text(pDX, IDC_NAME_DEVICE, m_NameDevice);
	DDV_MaxChars(pDX, m_NameDevice, 256);
	DDX_Text(pDX, IDC_NAME_LINE, m_NameLine);
	DDV_MaxChars(pDX, m_NameLine, 256);
	DDX_Text(pDX, IDC_NAME_PV1, m_NamePv1);
	DDV_MaxChars(pDX, m_NamePv1, 256);
	DDX_Text(pDX, IDC_NAME_PV2, m_NamePv2);
	DDV_MaxChars(pDX, m_NamePv2, 256);
	DDX_Text(pDX, IDC_NAME_PV3, m_NamePv3);
	DDV_MaxChars(pDX, m_NamePv3, 256);
	DDX_Text(pDX, IDC_NAME_PV4, m_NamePv4);
	DDV_MaxChars(pDX, m_NamePv4, 256);
	DDX_Text(pDX, IDC_NAME_STATION, m_NameStation);
	DDV_MaxChars(pDX, m_NameStation, 256);
	DDX_Text(pDX, IDC_NAME_TASK, m_NameTask);
	DDV_MaxChars(pDX, m_NameTask, 256);
	DDX_Text(pDX, IDC_PVI_PARAM_PV1, m_ParamPv1);
	DDV_MaxChars(pDX, m_ParamPv1, 256);
	DDX_Text(pDX, IDC_PVI_PARAM_PV2, m_ParamPv2);
	DDV_MaxChars(pDX, m_ParamPv2, 256);
	DDX_Text(pDX, IDC_PVI_PARAM_PV3, m_ParamPv3);
	DDV_MaxChars(pDX, m_ParamPv3, 256);
	DDX_Text(pDX, IDC_PVI_PARAM_PV4, m_ParamPv4);
	DDV_MaxChars(pDX, m_ParamPv4, 256);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPviObjectsDlg, CDialog)
	//{{AFX_MSG_MAP(CPviObjectsDlg)
	ON_BN_CLICKED(IDC_DEFAULT_SERIAL, OnDefaultSerial)
	ON_BN_CLICKED(IDC_DEFAULT_ETHERNET, OnDefaultEthernet)
	ON_BN_CLICKED(IDC_DEFAULT_ARSIM, OnDefaultARsim)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPviObjectsDlg message handlers

void CPviObjectsDlg::OnOK() 
{
	// eliminate leading and trailing whitespace characters:
	m_NameLine.TrimRight ();
	m_NameLine.TrimLeft ();
	m_NameDevice.TrimRight ();
	m_NameDevice.TrimLeft ();
	m_NameStation.TrimRight ();
	m_NameStation.TrimLeft ();
	m_NameCpu.TrimRight ();
	m_NameCpu.TrimLeft ();
	m_NameTask.TrimRight ();
	m_NameTask.TrimLeft ();
	m_NamePv1.TrimRight ();
	m_NamePv1.TrimLeft ();
	m_NamePv2.TrimRight ();
	m_NamePv2.TrimLeft ();
	m_NamePv3.TrimRight ();
	m_NamePv3.TrimLeft ();
	m_NamePv4.TrimRight ();
	m_NamePv4.TrimLeft ();
	m_ConnectLine.TrimRight ();
	m_ConnectLine.TrimLeft ();
	m_ConnectDevice.TrimRight ();
	m_ConnectDevice.TrimLeft ();
	m_ConnectStation.TrimRight ();
	m_ConnectStation.TrimLeft ();
	m_ConnectCpu.TrimRight ();
	m_ConnectCpu.TrimLeft ();
	m_ConnectTask.TrimRight ();
	m_ConnectTask.TrimLeft ();
	m_ConnectPv1.TrimRight ();
	m_ConnectPv1.TrimLeft ();
	m_ConnectPv2.TrimRight ();
	m_ConnectPv2.TrimLeft ();
	m_ConnectPv3.TrimRight ();
	m_ConnectPv3.TrimLeft ();
	m_ConnectPv4.TrimRight ();
	m_ConnectPv4.TrimLeft ();
	
	CDialog::OnOK();
}

void CPviObjectsDlg::OnDefaultSerial() 
{
	SetDefaultSerial ();
	UpdateData (FALSE);
}

void CPviObjectsDlg::OnDefaultEthernet() 
{
	SetDefaultEthernet ();
	UpdateData (FALSE);
}

void CPviObjectsDlg::OnDefaultARsim() 
{
	SetDefaultARsim ();
	UpdateData (FALSE);
}

//////////////////////////////////////////////////////////////////////////////
